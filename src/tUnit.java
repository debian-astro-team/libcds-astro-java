package cds.astro ;

import java.io.*;
import java.util.*;
import cds.astro.*;


/**
 * Testing the Unit class.
 * Dependencies: Unit, Udef, Editing, Parsing, Astroformat.
 * @author Francois Ochsenbein, Pierre Fernique [CDS]
 * @version 1.0 : 04-Mar-2000
 */
public class tUnit {
  /* Execute one interpretation and print the result */
  static void show (Unit u) {
        System.out.println("-------------------------------------------------");
	System.out.println("  Edited as: " + u);
	System.out.println("  editedVal: " + u.editedValue());
	System.out.println("     number: " + u.getValue());
	System.out.println("    explain: " + u.explainUnit());
	System.out.println("        Dim: " + u.dimension());
	System.out.println("       inSI: " + u.toStringInSI());
	System.out.println("     unitSI: " + u.getSIunit());
        System.out.println("-------------------------------------------------");
  }


//* TEST -----------------------------------------
  static public void main(String[] argv)  throws IOException {
	  BufferedReader stdin = new
			BufferedReader(new InputStreamReader(System.in));
	  Unit u = new Unit() ;
	  Unit u1 = new Unit() ;
	  Unit u2 = new Unit() ;
	  String line, pic, explanation, getCR;
	  Enumeration list;
	  Astrotime t = new Astrotime();
	  int mjd, y,m, d;
	  double val;
	  StringBuffer buf = new StringBuffer();
	  Editing ed = new Editing();

	/* Try Astrotime */
	line = "11 Sep 2001";
	Parsing p = new Parsing(line);
	mjd = p.parseDate(); System.out.print("...."+line+"=>"+p.form()+mjd);
	buf.setLength(0); ed.editDate(buf, mjd); System.out.println(" = "+buf);
	line = "1858.11.17"; p = new Parsing(line);
	mjd = p.parseDate(); System.out.print("...."+line+"=>"+p.form()+mjd);
	buf.setLength(0); ed.editDate(buf, mjd); System.out.println(" = "+buf);
	line = "-5000.11.1"; p = new Parsing(line);
	mjd = p.parseDate(); System.out.print("...."+line+"=>"+p.form()+mjd);
	buf.setLength(0); ed.editDate(buf, mjd); System.out.println(" = "+buf);
	line = "2005/Oct"; p = new Parsing(line);
	mjd = p.parseDate(); System.out.print("...."+line+"=>"+p.form()+mjd);
	buf.setLength(0); ed.editDate(buf, mjd); System.out.println(" = "+buf);
	line = "2000-001.5"; p = new Parsing(line);
	mjd = p.parseDate(); System.out.print("...."+line+"=>"+p.form()+mjd+"["+p+"]");
	buf.setLength(0); ed.editDate(buf, mjd); System.out.println(" = "+buf);

	/* Try the Date Parsing */
	line = "MJD50000.125";
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("\n...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "11 Sep 2001 13:01:02"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("\n...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "1858.11.17.12345"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-5000.11.1"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "03Sep2006.22:33:44.55"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "2000-001.5"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "2001/Oct"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "1850.10.31.12345"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "1900.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "1800.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "0001.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "0000.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-000.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-001.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-4710.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-4711.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-4712.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);
	line = "-4713.11.01"; 
	try { t.set(line); } catch (Exception e) { System.out.println(e); }
	System.out.println("...Astrotime("+line+") => " + t +", MJD"+t.mjd);

	y=1600; m=0; d=365; val=Astrotime.YMD2JD(y,m,d)-2400000.5;
	System.out.println("....MJD("+y+","+m+","+d+")="+val);
	y=1900; m=0; d=365; val=Astrotime.YMD2JD(y,m,d)-2400000.5;
	System.out.println("....MJD("+y+","+m+","+d+")="+val);
	y=1900; m=0; d=366; val=Astrotime.YMD2JD(y,m,d)-2400000.5;
	System.out.println("....MJD("+y+","+m+","+d+")="+val);
	y=1901; m=0; d=1; val=Astrotime.YMD2JD(y,m,d)-2400000.5;
	System.out.println("....MJD("+y+","+m+","+d+")="+val);
	y=1901; m=2; d=28; val=Astrotime.YMD2JD(y,m,d)-2400000.5;
	System.out.println("....MJD("+y+","+m+","+d+")="+val);
	y=2500; m=11; d=1; val=Astrotime.YMD2JD(y,m,d)-2400000.5;
	System.out.println("....MJD("+y+","+m+","+d+")="+val);

	System.out.print("\n====Will continue with JD/MJD units");
	getCR = stdin.readLine() ;
	line = "MJD50000.125";
	try { u1.set("MJD"); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	try { u2.set("JD"); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	try {
	    System.out.println("    set(" + line + ") gives:");
	    u.set(line);
	    u.dump("   (input)");
	    u.convertTo(u1);
	    u.dump("  (output)");
	    show(u);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	try {
	    System.out.println("\n----Another conversion:");
	    u.set(line);
	    u.dump("   (input)");
	    u.convertTo(u2);
	    u.dump("  (output)");
	    show(u);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	try {
	    System.out.println("\n----Conversion to \"datime:\"");
	    u2.set("\"datime\"");
	    u.set(line);
	    u.dump("   (input)");
	    u.convertTo(u2);
	    u.dump("  (output)");
	    show(u);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }


	System.out.print("\n====Will continue with 'picture' units");

	pic = "\"y-m-d\""; line = "2001-Sep-11.125";
	System.out.print("\n----pic=" + pic + "; input=" + line);
	getCR = stdin.readLine() ;
	p.set(line);
	try { val = p.parseComplex(pic); 
	      System.out.println("....parse("+pic+") => "+val); 
	      buf.setLength(0); ed.editComplex(buf, val, pic);
	      System.out.println("    ======>" + buf);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	System.out.println("----Execute now  u1.set(" + pic+line + ")");
	try { u1.set(pic+line); System.out.println("....Unit("+line+")="+u1); 
		show(u1); System.out.println(""); } 
	catch (Exception e) { System.out.println(e); }

	pic = "\"yyyymmdd\""; line = "20010911";
	System.out.print("\n----pic=" + pic + "; input=" + line);
	getCR = stdin.readLine() ;
	p.set(line);
	try { val = p.parseComplex(pic); 
	      System.out.println("....parse("+pic+") => "+val); 
	      buf.setLength(0); ed.editComplex(buf, val, pic);
	      System.out.println("    ======>" + buf);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	try { u1.set(pic+line); System.out.println("....Unit("+line+")="+u1); 
		show(u1); System.out.println(""); } 
	catch (Exception e) { System.out.println(e); }

	pic = "\"DD/MM/YYYY\""; line = "11/ 9/2001";
	System.out.print("\n----pic=" + pic + "; input=" + line);
	getCR = stdin.readLine() ;
	p.set(line);
	try { val = p.parseComplex(pic); 
	      System.out.println("....parse("+pic+") => "+val); 
	      buf.setLength(0); ed.editComplex(buf, val, pic);
	      System.out.println("    ======>" + buf);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	try { u1.set(pic+line); System.out.println("....Unit("+line+")="+u1); 
		show(u1); System.out.println(""); } 
	catch (Exception e) { System.out.println(e); }

	pic = "\"DD/MM/yy\""; line = "11/ 9/01";
	System.out.print("\n----pic=" + pic + "; input=" + line);
	getCR = stdin.readLine() ;
	p.set(line);
	try { val = p.parseComplex(pic); 
	      System.out.println("....parse("+pic+") => "+val); 
	      buf.setLength(0); ed.editComplex(buf, val, pic);
	      System.out.println("    ======>" + buf);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	try { u1.set(pic+line); System.out.println("....Unit("+line+")="+u1); 
		show(u1); System.out.println(""); } 
	catch (Exception e) { System.out.println(e); }

	/* Edit the default Unit */
	u = new Unit();
	System.out.println("\n....DefaultUnit=" + u) ;

	/* Add a new Definition */
	System.out.print("\n....Adding new unit: Rayleigh");
	getCR = stdin.readLine() ;
	try { Unit.addSymbol("Rayleigh", "10+6/(4*pi).ph.cm-2.s-1.sr-1"); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	System.out.print("====Additional unit added.");

	line = "6.28Rayleigh";
	System.out.print("\n....Asking now for: " + line);
	getCR = stdin.readLine() ;
	try   {
	    u.set(line);
	    u.dump(line);
	    show(u);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }

	/* Try the degrees */
	System.out.print("\n....Trying the definition of Degrees");
	getCR = stdin.readLine() ;
	try { u1.set("degC"); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	try { u2.set("degF"); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	try {
	    u.set("0K");
	    u.convertTo(u1);		// 0K in Celsius
	    show(u);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }
	try {
	    u.set("0K");
	    u.convertTo(u2);		// 0K in Fahrenheit
	    show(u);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }

	System.out.print("\n====Will continue with Sexagesimal");
	getCR = stdin.readLine() ;
	/* Try the Sexagesimal */
	System.out.println("\n....Trying the definition of Degrees");
	System.out.println("");
	try { u1.set("\"h:m:s\""); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	try { u2.set("\"d:m:s\""); }
	catch (Exception e) { System.out.println(e) ; e.printStackTrace(); }
	u1.dump("u1 ");
	u2.dump("u2 ");
	line = "23:0.";
	try {
		 u1.setValue(line);
		 u1.dump(line);
		 System.out.println("\nu1 contents from value: " + line);
		 show(u1);
		 Unit.convert(u1, u2);
		 System.out.println("Converted: " + u1 + " => " + u2);
		 System.out.println("\nu2 contents, value=" + u2.editedValue());
		 show(u2);
	}
	catch (Exception e) { System.out.println(e); e.printStackTrace(); }


	System.out.print("\n====Will list all Symbols:");
	getCR = stdin.readLine() ;
	list = Unit.symbols();
	// System.out.println("list: " + list);
	while (list.hasMoreElements()) {
		 line = (String)list.nextElement();
		 System.out.print(line + "\t");
		 System.out.println(Unit.explainUnit(line));
		 // Unit.explain((String)list.nextElement());
	}

		/* Loop on getting positions, and convert */
	while(true) {
		System.out.print("\nGive a Number with Unit to interpret: ") ;
		 line = stdin.readLine() ; if (line == null) break;

		 try   { u.set(line); }
		 catch (Exception e) {
		    System.out.println(e) ;
		    e.printStackTrace();
		    continue;
		 }
		 u.dump("..0..");
		 show(u);

		 u1.set(u);
		 try { u1.log();  } 
		 catch (Exception e) { System.out.println(e); }
		 u1.dump(".Log."); System.out.println("Edited value=" + u1);

		 try { u1.dexp();  } 
		 catch (Exception e) { System.out.println(e); }
		 u1.dump(".Exp."); System.out.println("Edited value=" + u1);

		 try { u1.power(2);  } 
		 catch (Exception e) { System.out.println(e); }
		 u1.dump("..^2."); System.out.println("Edited value=" + u1);
		 try { u1.sqrt();  } 
		 catch (Exception e) { System.out.println(e); }
		 u1.dump(".sqrt"); System.out.println("Edited value=" + u1);


		System.out.print("Give a 2nd Unit with Unit to interpret: ") ;
		 line = stdin.readLine() ;
		 try   { u2.set(line); }
		 catch (Exception e) { System.out.println(e) ; continue; }
		 u2.dump("..2..");
		 show(u2);

		 u1.set(u);
		 try { u1.sum(u2) ; } 
		 catch (Exception e) { System.out.println(e); }
		 u1.dump(".Sum.");
		 show(u1);

		 u1.set(u);
		 try { u1.plus(u2) ; }
		 catch (Exception e) { System.out.println(e); }
		 u1.dump(".Plus");
		 show(u1);


		}
  	   	System.out.print("\n");
	 }
}
